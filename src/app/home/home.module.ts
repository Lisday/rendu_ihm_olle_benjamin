import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { UserListComponent } from 'src/app/home/user-list/user-list.component';
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';
import { AddUserComponent } from '../add-user/add-user.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule
  ],
  declarations: [HomePage, UserListComponent, AddUserComponent]
})
export class HomePageModule {}
