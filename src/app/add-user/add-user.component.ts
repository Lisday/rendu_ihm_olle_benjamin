import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { isContext } from 'vm';
import { UserService } from '../api/user.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
})
export class AddUserComponent implements OnInit {

  name: string;
  job: string;
  avatar = '../icone.jpg';

  constructor(
    public router: Router,
    public userService: UserService) {
    
   }

  ngOnInit() {}

  onSubmit() {
    this.userService.createUser(this.name, this.job, this.avatar);
    this.router.navigate(['/home']);
  }
}
